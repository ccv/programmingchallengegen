package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
)

type challengeList struct {
	List []Challenge `json:"challenges"`
}

type Challenge struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Difficulty string `json:"dif"`
	Category   string `json:"cat"`
}

func main() {

	fmt.Println("--------------------------------")
	fmt.Println("-Pro/g/ramming Challenges, v4.0-")
	fmt.Println("--------------------------------")

	challengeFile, err := os.Open("challenges.json")
	if err != nil {
		log.Fatal("Cannot find challenges.json")
	}
	defer challengeFile.Close()

	challFile, _ := ioutil.ReadAll(challengeFile)

	var chal challengeList

	json.Unmarshal(challFile, &chal)

	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(len(chal.List))))
	if err != nil {
		log.Fatal(err)
	}

	n := nBig.Int64()

	fmt.Println("ID: ", chal.List[n].ID)
	fmt.Println("Name: ", chal.List[n].Name)
	fmt.Println("Difficulty: ", chal.List[n].Difficulty)
	fmt.Println("Category: ", chal.List[n].Category)
}
